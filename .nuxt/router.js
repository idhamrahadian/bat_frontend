import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _e3c131b2 = () => interopDefault(import('../pages/about.vue' /* webpackChunkName: "pages/about" */))
const _b32a1506 = () => interopDefault(import('../pages/berita.vue' /* webpackChunkName: "pages/berita" */))
const _a6b4f414 = () => interopDefault(import('../pages/cart.vue' /* webpackChunkName: "pages/cart" */))
const _5326861c = () => interopDefault(import('../pages/checkout.vue' /* webpackChunkName: "pages/checkout" */))
const _f341e48c = () => interopDefault(import('../pages/contact.vue' /* webpackChunkName: "pages/contact" */))
const _8985e3f4 = () => interopDefault(import('../pages/error404.vue' /* webpackChunkName: "pages/error404" */))
const _5015ff0a = () => interopDefault(import('../pages/farmers.vue' /* webpackChunkName: "pages/farmers" */))
const _03d377ec = () => interopDefault(import('../pages/gallery.vue' /* webpackChunkName: "pages/gallery" */))
const _0d28a4f6 = () => interopDefault(import('../pages/index2.vue' /* webpackChunkName: "pages/index2" */))
const _0d36bc77 = () => interopDefault(import('../pages/index3.vue' /* webpackChunkName: "pages/index3" */))
const _0d44d3f8 = () => interopDefault(import('../pages/index4.vue' /* webpackChunkName: "pages/index4" */))
const _0d52eb79 = () => interopDefault(import('../pages/index5.vue' /* webpackChunkName: "pages/index5" */))
const _88db4950 = () => interopDefault(import('../pages/layanan.vue' /* webpackChunkName: "pages/layanan" */))
const _0134f7ae = () => interopDefault(import('../pages/news.vue' /* webpackChunkName: "pages/news" */))
const _7c270957 = () => interopDefault(import('../pages/news_detail.vue' /* webpackChunkName: "pages/news_detail" */))
const _2b06b709 = () => interopDefault(import('../pages/product.vue' /* webpackChunkName: "pages/product" */))
const _16f41e76 = () => interopDefault(import('../pages/product-detail.vue' /* webpackChunkName: "pages/product-detail" */))
const _35039750 = () => interopDefault(import('../pages/projects.vue' /* webpackChunkName: "pages/projects" */))
const _3b292760 = () => interopDefault(import('../pages/projects_detail.vue' /* webpackChunkName: "pages/projects_detail" */))
const _10fe5dcf = () => interopDefault(import('../pages/service.vue' /* webpackChunkName: "pages/service" */))
const _15f03f3f = () => interopDefault(import('../pages/service-detail.vue' /* webpackChunkName: "pages/service-detail" */))
const _1ae186eb = () => interopDefault(import('../pages/struktur_organisasi.vue' /* webpackChunkName: "pages/struktur_organisasi" */))
const _faa6a46e = () => interopDefault(import('../pages/tugas_dan_fungsi.vue' /* webpackChunkName: "pages/tugas_dan_fungsi" */))
const _1265eea9 = () => interopDefault(import('../pages/why_choose_us.vue' /* webpackChunkName: "pages/why_choose_us" */))
const _a4517c28 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/about",
    component: _e3c131b2,
    name: "about"
  }, {
    path: "/berita",
    component: _b32a1506,
    name: "berita"
  }, {
    path: "/cart",
    component: _a6b4f414,
    name: "cart"
  }, {
    path: "/checkout",
    component: _5326861c,
    name: "checkout"
  }, {
    path: "/contact",
    component: _f341e48c,
    name: "contact"
  }, {
    path: "/error404",
    component: _8985e3f4,
    name: "error404"
  }, {
    path: "/farmers",
    component: _5015ff0a,
    name: "farmers"
  }, {
    path: "/gallery",
    component: _03d377ec,
    name: "gallery"
  }, {
    path: "/index2",
    component: _0d28a4f6,
    name: "index2"
  }, {
    path: "/index3",
    component: _0d36bc77,
    name: "index3"
  }, {
    path: "/index4",
    component: _0d44d3f8,
    name: "index4"
  }, {
    path: "/index5",
    component: _0d52eb79,
    name: "index5"
  }, {
    path: "/layanan",
    component: _88db4950,
    name: "layanan"
  }, {
    path: "/news",
    component: _0134f7ae,
    name: "news"
  }, {
    path: "/news_detail",
    component: _7c270957,
    name: "news_detail"
  }, {
    path: "/product",
    component: _2b06b709,
    name: "product"
  }, {
    path: "/product-detail",
    component: _16f41e76,
    name: "product-detail"
  }, {
    path: "/projects",
    component: _35039750,
    name: "projects"
  }, {
    path: "/projects_detail",
    component: _3b292760,
    name: "projects_detail"
  }, {
    path: "/service",
    component: _10fe5dcf,
    name: "service"
  }, {
    path: "/service-detail",
    component: _15f03f3f,
    name: "service-detail"
  }, {
    path: "/struktur_organisasi",
    component: _1ae186eb,
    name: "struktur_organisasi"
  }, {
    path: "/tugas_dan_fungsi",
    component: _faa6a46e,
    name: "tugas_dan_fungsi"
  }, {
    path: "/why_choose_us",
    component: _1265eea9,
    name: "why_choose_us"
  }, {
    path: "/",
    component: _a4517c28,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
